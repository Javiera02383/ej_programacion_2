PROGRAMA 
c) Leer una serie de números, determinar e imprimir el número más grande. El primer número leído indica
cuántos números deben procesarse.

00	1026	//Lee un número y lo almacena en la ubicación 26
01	2026	//Carga el número almacenado en la ubicación 26
02	4100	//Bifurcación negativa a la ubicación 00
03	2026	//Carga el número almacenado en la ubicación 26
04	4200	//Bifurcación Cero a la ubicación 00 
05	1024	//Lee un número y almacena el mayor en la ubicación 24
06	2025	//Carga el valor en el constructor, en la ubicación 25
07	3126	//Resta el valor en la ubicación 26
08	4221	//Bifurcación Cero a la ubicación 21
09	1023	//Recibe un número y lo almacena en la ubicación 23
10	2024	//Carga el número de la ubicación 24
11	3123	//Resta el número de la ubicación 23
12	4015	//Bifurca positivo a la ubicación 15
13	2023	//Carga el número almacenado en la ubicación 23
14	2124	//Almacena el número en la ubicación 24
15	2025	//Carga la variable del contador
16	3027	//Suma el contenido de la ubicación 27
17	2125	//Almacena el resultado en la ubicación 25
18	2025	//Carga el contador
19	3126	//Resta el contenido de la ubicación 26
20	4109	//Bifurca negativo a la ubicación 24
21	1124	//Imprime el numero mayor
22	4300	//Fin del programa 

23	0000	//Numero
24	0000	//Mayor 
25	0000	//Contador: Inicializado en 1
26	0000	//Limite
27	0000	//Uno: Inicializado en 1 e inicializado en 0

