package Capitulo_9;

import java.util.Scanner;

// Clase Punto para representar los puntos en cada figura
class Punto {
    private double x;
    private double y;

    public Punto(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}

// Clase Cuadrilatero (superclase)
class Cuadrilatero {
    private Punto punto1;
    private Punto punto2;
    private Punto punto3;
    private Punto punto4;

    public Cuadrilatero(Punto punto1, Punto punto2, Punto punto3, Punto punto4) {
        this.punto1 = punto1;
        this.punto2 = punto2;
        this.punto3 = punto3;
        this.punto4 = punto4;
    }

    public Punto getPunto1() {
        return punto1;
    }

    public Punto getPunto2() {
        return punto2;
    }

    public Punto getPunto3() {
        return punto3;
    }

    public Punto getPunto4() {
        return punto4;
    }

    public double calcularArea() {
        // Implementa el cálculo del área del cuadrilátero aquí
        return 0.0;
    }
}

// Clase Trapezoide (subclase de Cuadrilatero)
class Trapezoide extends Cuadrilatero {
    public Trapezoide(Punto punto1, Punto punto2, Punto punto3, Punto punto4) {
        super(punto1, punto2, punto3, punto4);
    }

    @Override
    public double calcularArea() {
        // Cálculo del área del trapecio
        double baseMayor = Math.abs(getPunto1().getX() - getPunto2().getX());
        double baseMenor = Math.abs(getPunto3().getX() - getPunto4().getX());
        double altura = Math.abs(getPunto1().getY() - getPunto3().getY());
        return (baseMayor + baseMenor) * altura / 2;
    }
}

// Clase Paralelogramo (subclase de Cuadrilatero)
class Paralelogramo extends Cuadrilatero {
    public Paralelogramo(Punto punto1, Punto punto2, Punto punto3, Punto punto4) {
        super(punto1, punto2, punto3, punto4);
    }

    @Override
    public double calcularArea() {
        // Cálculo del área del paralelogramo
        double base = Math.abs(getPunto1().getX() - getPunto2().getX());
        double altura = Math.abs(getPunto1().getY() - getPunto3().getY());
        return base * altura;
    }
}

// Clase Rectángulo (subclase de Paralelogramo)
class Rectangulo extends Paralelogramo {
    public Rectangulo(Punto punto1, Punto punto2, Punto punto3, Punto punto4) {
        super(punto1, punto2, punto3, punto4);
    }

    @Override
    public double calcularArea() {
        // Cálculo del área del rectángulo
        double base = Math.abs(getPunto1().getX() - getPunto2().getX());
        double altura = Math.abs(getPunto1().getY() - getPunto3().getY());
        return base * altura;
    }
}

// Clase Cuadrado (subclase de Rectángulo)
class Cuadrado extends Rectangulo {
    public Cuadrado(Punto punto1, Punto punto2, Punto punto3, Punto punto4) {
        super(punto1, punto2, punto3, punto4);
    }

    @Override
    public double calcularArea() {
        // Cálculo del área del cuadrado
        double lado = Math.abs(getPunto1().getX() - getPunto2().getX());
        return lado * lado;
    }
}

// Clase principal
public class Main {
    public static void main(String[] args) {
        // Solicitar al usuario los puntos para crear los objetos
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese los puntos para el Trapezoide (x1 y1 x2 y2 x3 y3 x4 y4):");
        Punto punto1 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        Punto punto2 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        Punto punto3 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        Punto punto4 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        Trapezoide trapezoide = new Trapezoide(punto1, punto2, punto3, punto4);

        System.out.println("Ingrese los puntos para el Paralelogramo (x1 y1 x2 y2 x3 y3 x4 y4):");
        punto1 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto2 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto3 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto4 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        Paralelogramo paralelogramo = new Paralelogramo(punto1, punto2, punto3, punto4);

        System.out.println("Ingrese los puntos para el Rectángulo (x1 y1 x2 y2 x3 y3 x4 y4):");
        punto1 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto2 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto3 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto4 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        Rectangulo rectangulo = new Rectangulo(punto1, punto2, punto3, punto4);

        System.out.println("Ingrese los puntos para el Cuadrado (x1 y1 x2 y2 x3 y3 x4 y4):");
        punto1 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto2 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto3 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        punto4 = new Punto(scanner.nextDouble(), scanner.nextDouble());
        Cuadrado cuadrado = new Cuadrado(punto1, punto2, punto3, punto4);

        // Calcular y mostrar el área de cada objeto
        System.out.println("Área del Trapezoide: " + trapezoide.calcularArea());
        System.out.println("Área del Paralelogramo: " + paralelogramo.calcularArea());
        System.out.println("Área del Rectángulo: " + rectangulo.calcularArea());
        System.out.println("Área del Cuadrado: " + cuadrado.calcularArea());
        scanner.close();
    }
}
